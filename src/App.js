import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import AdminDashboard from './pages/AdminDashboard';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import OrderDetails from './pages/OrderDetails';
import Products from './pages/Products';
import Register from './pages/Register';
import AddProduct from './pages/AddProduct';
import ProductView from './pages/ProductView';
// import UserOrderHistory from './pages/UserOrderHistory';
import './App.css';
import { UserProvider } from './UserContext';

function App() {
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
    // email: localStorage.getItem('email')
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }

    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {

          setUser({
              id: data._id,
              isAdmin: data.isAdmin
          })
      } else {
        setUser({
            id: null,
            isAdmin: null
        })
      }

    })
  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
            <Routes>
              <Route path="/" element={<Home/>}  />
              <Route path="/addproduct" element={<AddProduct/>}  />

              <Route path="/adminDashboard" element={<AdminDashboard/>}  />
              <Route path="/products" element={<Products/>} />
              <Route path="/products/:productId" element={<ProductView/>} />
              {/*<Route path="/myOrders" element={<UserOrderHistory/>} />*/}
              
              
              <Route path="/login" element={<Login/>} />
              <Route path="/logout" element={<Logout/>} />
              <Route path="/orderDetails" element={<OrderDetails/>} />
              <Route path="/productView" element={<ProductView/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/*" element={<Error/>} />
            </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
