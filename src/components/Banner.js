import { Link } from 'react-router-dom';
import {Row, Col, Button, Card} from 'react-bootstrap';
import '../App.css';


export default function Banner({data}) {

	const { title, content, destination, label, image } = data;

	return (

			<>
				<Card className="mt-4">	
						<Card.Body>
						 <Card.Title>{title}</Card.Title>
				         <Card.Img  variant="top" src={image} width="600" height="400"/>
				          <Card.Text className="text-justify">
				            {content}
				          </Card.Text>
				         <Button className="button-glow" as={ Link } to={destination}>{label}</Button>
				        </Card.Body>
				</Card>
			</>

		)
}
