import React, { useState, useEffect,useContext } from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';
import {Row, Col, Table, Button, Modal, Form} from 'react-bootstrap';
import '../App.css';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function TableContent({productProp}) {

	const {name, description, price, stocks, isActive, _id} = productProp;
	const [products, setProducts] = useState([]);
	const navigate = useNavigate();

	const { user } = useContext(UserContext);
	const [show, setShow] = useState(false);
	const { productId } = useParams();
	const [status, setStatus] = useState(false);

	const [updateName, setupdateName] = useState(name);
	const [updateDescription, setupdateDescription] = useState(description);
	const [updatePrice, setupdatePrice] = useState(price);
	const [updateStocks, setupdateStocks] = useState(stocks);
	const [availability, setAvailability] = useState(isActive);



	 const handleClose = () => setShow(false);
	 const handleShow = () => setShow(true);

	 function updateProductInfo(e) {

	 	fetch(`${process.env.REACT_APP_API_URL}/products/update/${_id}`, {
	 		method: 'PUT',
	 		headers: {
	 			Authorization: `Bearer ${localStorage.getItem('token')}`,
	 			'Content-Type': 'application/json'
	 		},
	 		body: JSON.stringify({
	 			name: updateName,
	 			description: updateDescription,
	 			price: updatePrice,
	 			stocks: updateStocks
	 		})
	 	})
	 	.then(res => res.json())
	 	.then(data => {
	 		console.log(data)

	 			if(data === true) {


	 				Swal.fire({
	 					title: "Updated",
	 					icon: "success",
	 					text: "Product has been updated successfully!"
	 				})
	 				
	 				handleClose();
	 				
	 			} else {
	 				Swal.fire({
	 					title: "Something went wrong!",
	 					icon: "error",
	 					text: "Please check your input details "
	 				})
	 				
	 				
	 			}
	 		
	 	})
	 }


	 function activateProduct(e) {
	 	e.preventDefault()

	 	fetch(`${process.env.REACT_APP_API_URL}/products/active/${_id}`, {
	 		method: 'PUT',
	 		headers: {
	 			Authorization: `Bearer ${localStorage.getItem('token')}`,
	 			'Content-Type': 'application/json'
	 		},
	 		
	 	})
	 	.then(res => res.json())
	 	.then(data => {
	 		console.log(data)
	 		
	 	})

	 	setAvailability(true);
	 }

	 function archiveProduct(e) {
	 	e.preventDefault()

	 	fetch(`${process.env.REACT_APP_API_URL}/products/archive/${_id}`, {
	 		method: 'PUT',
	 		headers: {
	 			Authorization: `Bearer ${localStorage.getItem('token')}`,
	 			'Content-Type': 'application/json'
	 		},
	 		
	 	})
	 	.then(res => res.json())
	 	.then(data => {
	 		console.log(data)
	 		
	 	})

	 	setAvailability(false);
	 }

	 function deleteProduct(e) {
	 	e.preventDefault()

	 	fetch(`${process.env.REACT_APP_API_URL}/products/delete/${_id}`, {
	 		method: 'DELETE',
	 		headers: {
	 			Authorization: `Bearer ${localStorage.getItem('token')}`,
	 			'Content-Type': 'application/json'
	 		},
	 		
	 	})
	 	.then(res => res.json())
	 	.then(data => {
	 		console.log(data)
	 		
	 	})

	 }


	 useEffect(() => {
	 	if(updateName !== "" && updateDescription !== "" && updatePrice !== "" && updateStocks !== "") {
	 		setStatus(true)
	 		
	 	
	 	} else {
	 		setStatus(false)
	 		

	 	}
	 }, [updateName, updateDescription, updatePrice, updateStocks ])

	 

	
	return (

			<>
				<tbody>

				  <tr>
				    <td>{name}</td>
				    <td >{description}</td>
				    <td>{price}</td>
				    <td>{stocks}</td>
				    <td>
				    <Button className="button-glow" onClick={handleShow}>Update</Button>

				    <Modal show={show} onHide={handleClose}>
				            <Modal.Header closeButton>
				              <Modal.Title>Update Product</Modal.Title>
				            </Modal.Header>
				            <Modal.Body>
				              <Form>
					                <Form.Group className="mb-3" controlId="productName">
					                  <Form.Label>Product Name</Form.Label>
					                  <Form.Control
					                    type="text"
					                    placeholder={name}
					                    autoFocus
					                    value={updateName}
					                    onChange={e => setupdateName(e.target.value) }
					                    required
					                  />
					                </Form.Group>

					                <Form.Group
					                  className="mb-3"
					                  controlId="productDescription">
					                
					                  <Form.Label>Description</Form.Label>
					                  <Form.Control
					                   as="textarea" 
					                   rows={3} 
					                   placeholder={description}
					                   value={updateDescription}
					                   onChange={e => setupdateDescription(e.target.value) }
					                   required
					                   />
					                </Form.Group>

					                <Form.Group className="mb-3" controlId="productPrice">
					                  <Form.Label>Price</Form.Label>
					                  <Form.Control
					                    type="text"
					                    pattern="[0-9]+"
					                    placeholder={price}
					                    autoFocus
					                    value={updatePrice}
					                    onChange={e => setupdatePrice(e.target.value) }
					                    required
					                  />
					                </Form.Group>

					                <Form.Group className="mb-3" controlId="productStocks">
					                  <Form.Label>Stocks</Form.Label>
					                  <Form.Control
					                    type="text"
					                    pattern="[0-9]+"
					                    placeholder={stocks}
					                    autoFocus
					                    value={updateStocks}
					                    onChange={e => setupdateStocks(e.target.value) }
					                    required
					                  />
					                </Form.Group>

				              </Form>
				            </Modal.Body>
				            <Modal.Footer>
				              <Button variant="secondary" onClick={handleClose}>
				                Close
				              </Button>

				              {

				              	(status)?
				              		<Button variant="primary" onClick={e => updateProductInfo(e)} type="submit" id="submitBtn">
				              		  Save Changes
				              		</Button>
				              		:
				              		<Button variant="primary" disabled>
				              		  Save Changes
				              		</Button>

				              }

				            </Modal.Footer>
				          </Modal>

				    <br/>
				    <br/>
					    {
					    	(availability)?
					    		<Button className="secondary" variant="secondary" onClick={e => archiveProduct(e)}>Disable</Button>
					    		:
					    		<Button className="success" variant="success"  onClick={e => activateProduct(e)}>Enable</Button>

					    }
					 <br/>
					 <br/>
					 	<Button className="danger" variant="danger" onClick={e => deleteProduct(e)} >Delete</Button>
				    </td>

				  </tr>
				
				</tbody>
			</>
			  
			      
			  
			  

		)
}