import { Link } from 'react-router-dom';
import {Row, Col, Table, Button} from 'react-bootstrap';
import '../App.css';


export default function TableOrder({orderProp}) {

	const { firstName, lastName, orderedProduct,totalAmount, _id} = orderProp;
	const name = orderedProduct.map(({productName}) => {
		return (

				<>
					{productName}
					<br/>
				</>

			)
	})
	const quantity = orderedProduct.map(({quantity}) => {
		return (

				<>
					{quantity}
					<br/>
				</>

			)
	})
	const subtotal = orderedProduct.map(({subtotal}) => {
		return (

				<>
					{subtotal}
					<br/>
				</>

			)
	})
	return (

			<tbody>
				<tr >
					<td className="h-auto p-2 text-primary">{firstName} {lastName}</td>
					<td className="h-auto p-2 text-start">{name}</td>
					<td className="h-auto p-2 text-danger">{quantity}</td>
					<td className="h-auto p-2 text-start text-danger">{subtotal}</td>
					<td className="h-auto p-2 text-danger">{totalAmount}</td>
							
				</tr>
			</tbody>
			  
			      
			  
			  

		)
}