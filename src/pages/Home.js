import Banner from '../components/Banner';
import Products from './Products';




export default function Home() {

	const data = {
			title: "",
			image: "https://barn2.com/wp-content/uploads/2019/08/519946_FeaturedImage-OnlineStore_082819-820x273.png",
			content: "Online Shopping for Mobiles & Tablets with a huge variety of latest High Quality Phones",
			destination: "/register",
			label: "Get Started!"
		}

	return (

			<>
				<Banner data={data}/>
				<br/>
				<Products/>
				
				
			</>




		)
}