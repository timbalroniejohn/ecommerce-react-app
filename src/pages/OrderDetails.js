import { useState, useEffect, useContext } from 'react';
import { Form, Button, ButtonGroup, Table } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import TableOrder from '../components/TableOrder';
import UserContext from '../UserContext';



export default function OrderDetails() {

	const { user } = useContext(UserContext);
	const [orders, setOrders] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/allProfile`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setOrders(data.map(order => {
				return (
				<TableOrder key={order._id} orderProp={order}/>
				)
			}))
		})
	}, [orders]);


	return (
			(user.isAdmin === true) ?

			<div className="text-center">
				<h1> Order Details</h1>
				
				<br/>
				


				<Table striped bordered hover variant="dark">
				      <thead>
				        <tr>
				          <th className="w-25 p-1">User</th>
				          <th className="w-50 p-1">Ordered Product</th>
				          <th className="w-auto p-1">Quantity</th>
				          <th className="w-auto p-1">Subtotal</th>
				          <th className="w-auto p-1">Total</th>
				      
				        </tr>

				      </thead>
				      {orders}
				      
				    </Table>


			</div>
			:
			<Navigate to="/login"/>
		)
}